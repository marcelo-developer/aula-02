
public class Funcionario
{
    String nome;
    boolean ehTrabalhadorNoturno, ehVendedor, usaValeTransporte;
    double totalVendas, salarioBase,salarioLiquido;
    int numeroFilhos, auxiliar;
    
     
    public Funcionario(String vNome, boolean vTrabalhaNoite,
            boolean ehVend, boolean vUsaVale, double vVendas,
            double salario, int numFilhos){
        nome = vNome;
        ehTrabalhadorNoturno= vTrabalhaNoite;
        ehVendedor= ehVend;
        usaValeTransporte= vUsaVale;
        totalVendas= vVendas;
        salarioBase= salario;
        numeroFilhos = numFilhos;
    }
    
    public double salarioLiquido(){
            auxiliar = 13;
          if(ehTrabalhadorNoturno)
              auxiliar =  auxiliar -5;
          
          if(usaValeTransporte)
              auxiliar =   auxiliar + 3;
        if(ehVendedor)
              salarioLiquido = salarioBase + (totalVendas * 0.02);
        
            
        return salarioLiquido ;
    }
    
    public void setNome(String novoNome){
        nome = novoNome;
    }
    public String getNome(){
        return nome;
    }
    
    public void setEhTrabalhadorNoturno(boolean noite){
        ehTrabalhadorNoturno = noite;
    }
    public boolean getEhTrabalhadorNoturno(){
        return ehTrabalhadorNoturno;
    }
    
    public void setTotalVendas(double novoTotal){
        totalVendas = novoTotal;   
    }
    public double getTotalVendas(){
        return totalVendas;
    }
    
    public void setSalarioBase(double novoSalarioBase){
        salarioBase = novoSalarioBase;
    }
    public double getSalarioBase(){
        return salarioBase;
    }
    
    public void setNumeroFilhos(int novoNumeroFilhos){
        if(numeroFilhos >= 0)
            numeroFilhos = novoNumeroFilhos;
    }
    public int getNumeroFilhos(){
        return numeroFilhos;
    }
    public void setUsaValeTransporte(boolean valeTransporte){
         usaValeTransporte = valeTransporte;   
    }
    public boolean getUsaValeTransporte(){
        return usaValeTransporte;
    }
}
